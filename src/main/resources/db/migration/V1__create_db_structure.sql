CREATE TABLE IF NOT EXISTS images (
    id uuid NOT NULL,
    url VARCHAR(255),
    path VARCHAR(255),
    image_hash bigint,
    PRIMARY KEY (id)
);

CREATE OR REPLACE FUNCTION bit_count(value bigint)
    RETURNS numeric
AS $$ SELECT 1-SUM((value >> bit) & 1)/64 FROM generate_series(0, 63) bit $$
    LANGUAGE SQL IMMUTABLE STRICT;

