package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ImageRepository extends JpaRepository<Image, UUID> {

    Optional<Image> getById(UUID id);


    @Query(value = "SELECT CAST ( id AS varchar) AS ImageId, " +
            "bit_count(i.image_hash # :targetHash) as MatchPercent, " +
            "i.url as ImageUrl " +
            "FROM images i \n " +
            "WHERE bit_count(image_hash # :targetHash) > :threshold",
            nativeQuery = true)
    List<SearchResultDTO> search(@Param("targetHash")Long targetHash, @Param("threshold")double threshold);
}
