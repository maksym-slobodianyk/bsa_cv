package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.DiskFileSystem;
import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.entity.Image;
import bsa.java.concurrency.utils.Hasher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;

@Service
public class ImageService {

    private final FileSystem fileSystem;
    private final ImageRepository repository;
    private final Hasher hasher;

    @Autowired
    public ImageService(DiskFileSystem fileSystem, ImageRepository repository, Hasher hasher) {

        this.fileSystem = fileSystem;
        this.repository = repository;
        this.hasher = hasher;
    }

    public void processBatch(MultipartFile[] images) {
        Arrays.stream(images)
                .map(this::toBufferedImage)
                .filter(Objects::nonNull)
                .parallel()
                .forEach(i -> save(i));
    }

    public void deleteById(UUID id) {

        Optional<Image> imageToDelete = repository.getById(id);

        if (imageToDelete.isPresent()) {
            File fileToDelete = new File(imageToDelete.get().getPath());
            if (fileToDelete.exists()) {
                fileSystem.delete(fileToDelete);
                repository.deleteById(id);
            }
        }
    }

    public void deleteAll() {
        fileSystem.deleteAll();
        repository.deleteAll();
    }

    @Async
    public void save(BufferedImage image) {
        try {
            UUID generatedId = UUID.randomUUID();
            CompletableFuture<String> futurePath = fileSystem.saveImage(generatedId + ".png", image);
            Long hash = hasher.hash(image);
            String path = futurePath.get();
            repository.save(Image.builder()
                    .id(generatedId)
                    .url("http://localhost:8080/files/" + generatedId)
                    .path(path)
                    .hash(hash)
                    .build());

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Async
    public void save(MultipartFile file) {
        save(toBufferedImage(file));
    }

    public byte[] getImage(UUID id) throws IOException {
        Optional<Image> image = repository.getById(id);

        if (image.isPresent()) {

            File file = new File(image.get().getPath());

            if (file.exists())
                try (FileInputStream fis = new FileInputStream(file)) {
                    return fis.readAllBytes();
                } catch (IOException e) {
                    e.printStackTrace();
                    throw e;
                }
        }
        return new byte[0];
    }

    public List<SearchResultDTO> search(MultipartFile file, double threshold) {

        List<SearchResultDTO> result = repository.search(hasher.hash(toBufferedImage(file)), threshold);

        if (result.size() == 0)
            CompletableFuture.runAsync(() -> save(file));

        return result;
    }

    private BufferedImage toBufferedImage(MultipartFile file) {
        try {
            return ImageIO.read(file.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
