package bsa.java.concurrency.fs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

@Component
public class DiskFileSystem implements FileSystem {

    @Value("${file-storage.path}")
    String storagePath;

    @Override
    @Async
    public CompletableFuture<String> saveImage(String path, BufferedImage image) {

        path = storagePath + path;

        if (!Files.exists(Paths.get(storagePath)))
            new File(storagePath).mkdirs();

            try (var fos = new FileOutputStream(new File(path))) {

                ImageIO.write(image, "png", fos);

            } catch (IOException e) {
                e.printStackTrace();
            }
        return CompletableFuture.completedFuture(path);
    }
    @Override
    public void delete(File fileToDelete){
        fileToDelete.delete();
    }


    @Override
    public void deleteAll(){
        var directory = new File(storagePath);

        if (directory.exists()) {

            Arrays.stream(Objects.requireNonNull(directory.listFiles()))
                    .forEach(f -> {
                        if (f.isDirectory())
                            Arrays.stream(Objects.requireNonNull(f.listFiles()))
                                    .forEach(File::delete);
                        f.delete();
                    });

        }
    }

}
