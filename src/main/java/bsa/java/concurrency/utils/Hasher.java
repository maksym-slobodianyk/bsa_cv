package bsa.java.concurrency.utils;

import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.image.BufferedImage;

@Component
public class Hasher {

    public Long hash(BufferedImage image) {
        BufferedImage result = preprocess(image);
        long hash = 0;
        for (int row = 0; row < 8; row++) {
            for (int col = 1; col < 9; col++) {
                if (result.getRGB(col, row) > result.getRGB(col - 1, row))
                    hash |= 1;
                hash <<= 1;
            }
        }
        return hash;
    }

    private BufferedImage preprocess(BufferedImage source) {
        var result = source.getScaledInstance(9, 8, Image.SCALE_SMOOTH);
        var output = new BufferedImage(9, 8, BufferedImage.TYPE_BYTE_GRAY);
        output.getGraphics().drawImage(result, 0, 0, null);
        return output;
    }
}